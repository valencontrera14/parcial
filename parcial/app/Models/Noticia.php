<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;
use App\Models\Carrera;
use App\Models\Categoria;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Noticia extends Model
{
    
    use SoftDeletes;
    use HasFactory;
    protected $dates=['delete_at'];
    protected $fillable=['titulo','contenido','autor','carrera_id','categoria_id'];

    static public function conImagen()
    {
        return Noticia::whereNotNull('imagen');
    }

     public function creadaPor()
    {
        return $this->belongsTo(User::class,'autor');
    }
    public function categorizada()
    {
        return $this->belongsTo(Categoria::class,'categoria_id');
    }
    public function perteneceA()
    {
        return $this->belongsTo(Carrera::class,'carrera_id');
    }

    
}
