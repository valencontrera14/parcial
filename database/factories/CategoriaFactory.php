<?php

namespace Database\Factories;

use App\Models\Categoria;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoriaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Categoria::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $fecha=$this->faker->dateTimeBetween('-5 years');
        return [
            'nombre'=>$this->faker->word,
            'created_at'=>$fecha,
            'updated_at'=>$this->faker->dateTimeBetween($fecha),
            
        ];
    }
}
