<?php

namespace Database\Factories;

use App\Models\Noticia;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Categoria;
use App\Models\Carrera;

class NoticiaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Noticia::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    { 
        $fecha=$this->faker->dateTimeBetween('-5 years');
        return [
            
            'titulo'=> $this->faker->realText(25),
            'contenido'=>$this->faker->text(230),
            'autor'=>User::all()->random()->id,
            'carrera_id'=>Carrera::all()->random()->id,
            'categoria_id'=>Categoria::all()->random()->id,
            'imagen'=>$this->faker->optional()->imageUrl(400,200),
            'created_at'=>$fecha,
            'updated_at'=>$this->faker->dateTimeBetween($fecha),
            
        ];
    }

    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'imagen' => null,
            ];
        });
    }
}




