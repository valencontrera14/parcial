<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Noticia;

class Carrera extends Model
{
    use HasFactory;
    public function noticias()
    {
        return $this->hasMany(Noticia::class,'carrera_id');
    }
}
